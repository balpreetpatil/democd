require 'albacore'

BUILD_OUTPUT = './build-output'
BUILD_NUMBER = ENV['BUILD_NUMBER'] || '0'
GIT_COMMIT_SHA_ID = ENV['BUILD_VCS_NUMBER'] || '0'

NUGET_PATH = './tools/nuget/nuget.exe'
NUGET_SOURCE = 'http://get.nuget.ttldev'
NUGET_TTLPACKAGE_SOURCE = 'http://get.ttlpackages.ttldev'
NUGET_TTLPACKAGE_REPOSITORY = 'http://push.ttlpackages.ttldev'
NUGET_TTLPACKAGE_REPOSITORY_KEY = 'repo-ttlpackages-internal:uH34SddRf'

GO_USERNAME = ENV['GO_USERNAME'] || 'balpreetp'
GO_PASSWORD = ENV['GO_PASSWORD'] || 'Monday99!'

DEPLOYMENT_ENV_NAME  = ENV['DEPLOYMENT_ENV_NAME']
DEPLOYMENT_ENV_TYPE  = ENV['DEPLOYMENT_ENV_TYPE']

INCLUDE_BUILD_TOOLS_PRERELEASE = false

task :default => [:build]
task :build => [:set_version, :compile]
task :package => [:install_app_packager, :set_version, :create_packages]
task :publish => [:install_app_packager, :set_version, :publish_pre_replease]

task :set_version do
	BUILD_VERSION = IO.read('Version') % { :build_number => ('%03d' % BUILD_NUMBER) }
	raise "Invalid version number: #{version}. Format should be per semantic versioning v1 rules (http://semver.org/spec/v1.0.0.html) with any pre-release number padded to three digits." unless BUILD_VERSION =~ /^(?:\d\.\d\.\d)(-\D+\d{3})?$/
end

task :compile do
	puts 'Booking Service build successfull.'
end

task :test do
	puts 'Unit tests successfull.'
end

task :publish_pre_replease do
	puts 'Published pre release package'
end

task :create_staging_environment_deployment_map =>[:set_dm_version] do
	puts "Building staging deployment map version #{DM_VERSION}"
end

task :set_dm_version => [:set_version] do
	if BUILD_VERSION.include? '-'
		dm_build_counter = '%03d' % (ENV['DM_BUILD_COUNTER'] || '0')
		DM_VERSION = "#{BUILD_VERSION}#{dm_build_counter}"
	elsif INCLUDE_BUILD_TOOLS_PRERELEASE
		dm_build_counter = '%03d' % (ENV['DM_BUILD_COUNTER'] || '0')
		DM_VERSION = "#{BUILD_VERSION}-alpha#{dm_build_counter}"
	else
		dm_build_counter = ENV['DM_BUILD_COUNTER'] || '0'
		DM_VERSION = "#{BUILD_VERSION}.#{dm_build_counter}"
	end
end

desc 'Create tlpk and NuGet package'
task :create_packages do 
	puts "Created tlkp and nuget package"
end

exec :trigger_deployment_in_staging_environment => [:install_triggergobuild,:set_dm_version] do |ex|
	go_server_url = 'http://cnr.go.ttldev:8153'
	go_pipeline_name = "DeployStaging"
	#artifactory_url = 'get.ttlpackages.ttldev'
	#package_name = "TTL.DeploymentMap.#{ROLE_NAME}.#{DEPLOYMENT_ENV_TYPE}.#{DEPLOYMENT_ENV_NAME}"
	go_tracking_variable = 'TeamCityTrackingVariable'
	
	command = './packages/TTL.TriggerGoBuild/tools/TriggerGoBuild.exe'
	parameters = "-i #{go_server_url} -n #{go_pipeline_name} -u #{GO_USERNAME} -p #{GO_PASSWORD} -t #{go_tracking_variable} -a false -c #{DM_VERSION}"#-f #{artifactory_url} -g #{package_name}

	puts parameters
	puts "Deploying version #{DM_VERSION} of deployment map to staging environment"
	puts command
	puts parameters

	ex.command = command
	ex.parameters = parameters
end

task :run_acceptance_tests do
	puts "Running Acceptance tests"
end

exec :trigger_release_in_staging_environment => [:install_triggergobuild, :set_dm_version] do |ex|
	go_server_url = 'http://cnr.go.ttldev:8153'
	go_pipeline_name = 'SwitchActive_Staging'
	artifactory_url = 'get.ttlpackages.ttldev'	
	go_tracking_variable = 'TeamCityTrackingVariable'
	
	command = './packages/TTL.TriggerGoBuild/tools/TriggerGoBuild.exe'
	parameters = "-i #{go_server_url} -n #{go_pipeline_name} -u #{GO_USERNAME} -p #{GO_PASSWORD} -t #{go_tracking_variable} -a false -c #{DM_VERSION}"

	puts "Switching new version to be Active."
	puts command
	puts parameters

	ex.command = command
	ex.parameters = parameters
end
 
task :create_production_deployment_map =>[:set_dm_version] do
	puts "Building production deployment map version #{DM_VERSION}"
end

exec :trigger_deployment_in_production_environment => [:install_triggergobuild, :set_dm_version] do |ex|
	go_server_url = 'http://cnr.go.ttldev:8153'
	go_pipeline_name = 'DeployProduction'
	artifactory_url = 'get.ttlpackages.ttldev'
	package_name = "TTL.DeploymentMap"
	go_tracking_variable = 'TeamCityTrackingVariable'
	
	command = './packages/TTL.TriggerGoBuild/tools/TriggerGoBuild.exe'
	parameters = "-i #{go_server_url} -n #{go_pipeline_name} -u #{GO_USERNAME} -p #{GO_PASSWORD} -t #{go_tracking_variable} -a false -c #{DM_VERSION}"#-f #{artifactory_url} -g #{package_name} "

	puts "Deploying version #{DM_VERSION} of deployment map to production environment"
	puts command
	puts parameters

	ex.command = command
	ex.parameters = parameters
end

task :post_deployment_health_check do
	puts "Health check of deployment. Our service endpoint, services we depends, database or queues etc. are reachable"
end

exec :trigger_release_in_production_env => [:install_triggergobuild, :set_dm_version] do |ex|
	go_server_url = 'http://cnr.go.ttldev:8153'
	go_pipeline_name = 'SwitchActive_Production'
	artifactory_url = 'get.ttlpackages.ttldev'
	package_name = ""
	go_tracking_variable = 'TeamCityTrackingVariable'
	
	command = './packages/TTL.TriggerGoBuild/tools/TriggerGoBuild.exe'
	parameters = "-i #{go_server_url} -n #{go_pipeline_name} -u #{GO_USERNAME} -p #{GO_PASSWORD} -t #{go_tracking_variable} -a false -c #{DM_VERSION}"#-f #{artifactory_url} -g #{package_name} "

	puts "Switching new version to be Active."
	puts command
	puts parameters

	ex.command = command
	ex.parameters = parameters
end
 
desc 'Install App Packager'
nugetinstall :install_app_packager do |nuget|
	nuget.command = NUGET_PATH
	nuget.package = 'Trainline.AppPackager'
	nuget.sources = [NUGET_SOURCE]
	nuget.output_directory = './packages'
	nuget.exclude_version = true
	nuget.no_cache = true
	nuget.prerelease = INCLUDE_BUILD_TOOLS_PRERELEASE
end

nugetinstall :install_triggergobuild do |nuget|
	nuget.command = NUGET_PATH
	nuget.package = 'TTL.TriggerGoBuild'
	nuget.sources = [NUGET_SOURCE]
	nuget.output_directory = './packages'
	nuget.exclude_version = true
	nuget.no_cache = true
	nuget.prerelease = INCLUDE_BUILD_TOOLS_PRERELEASE
end
